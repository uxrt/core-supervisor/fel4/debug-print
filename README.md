# debug-println

This crate provides debug_println!() and debug_print!() macros that can be enabled and disabled at runtime.

## Usage

The debug_println!()/debug_print!() macros are the same as println!()/print!() 
except that they have no effect by default and only print when they are enabled
with the enable_debug_print() function. It may be disabled with the
disable_debug_print() function. The debug_print_is_enabled() function allows
checking whether debug printing is currently enabled.

## License
This crate is licensed under MIT. See LICENSE for details.
