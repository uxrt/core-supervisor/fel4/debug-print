#![no_std]

#[macro_use]
extern crate sel4;
pub use sel4::DebugOutHandle;

static mut debug_print_enabled: bool = false;

pub fn enable_debug_print(){
    unsafe { debug_print_enabled = true; }
}

pub fn disable_debug_print(){
    unsafe { debug_print_enabled = false; }
}

pub fn debug_print_is_enabled() -> bool {
    unsafe { debug_print_enabled }
}

#[macro_export]
macro_rules! debug_println {
    ($($toks:tt)*) => ({
        use ::core::fmt::Write;
        use ::debug_print::debug_print_is_enabled;
        if debug_print_is_enabled() {
            let _ = writeln!($crate::DebugOutHandle, $($toks)*);
        }
    })
}

#[macro_export]
macro_rules! debug_print {
    ($($toks:tt)*) => ({
        use ::core::fmt::Write;
        use ::debug_print::debug_print_is_enabled;
        if debug_print_is_enabled() {
            let _ = write!($crate::DebugOutHandle, $($toks)*);
        }
    })
}
